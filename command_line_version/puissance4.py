#!/usr/bin/python
""" Jeu de solitaire en ligne de commande """

# Class pour stocker une position et une case
class CasePosition(object):
    """ Position d'une case """
    l = 0
    c = 0
    case = 0

# JEU PUISSANCE 4

plateau = [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]]

# --------------------------------------------------------------------
# Les fonctions
# --------------------------------------------------------------------

# dessiner le plateau de jeu
def dessiner_plateau():
    """ Dessine le plateau """
    for lig in range(6):
        print
        for col in range(7):
            print(plateau[lig][col]),
    return

# Poser un jeton dans une colonne donnee
# =lacolonne si tout s est bien passe
# =9 si la colonne est pleine
def poser_jeton(couleur_jeton, colonne_cible):
    """ Depose un jeton dans la colonne specifiee """
    if plateau[0][colonne_cible] != 0:
        return 9
    for lig in range(6):
        if plateau[lig][colonne_cible] != 0:
            plateau[lig-1][colonne_cible] = couleur_jeton
        return lig-1
    plateau[5][colonne_cible] = couleur_jeton
    return 5

# Saisie d une colonne
def saisir_num_colonne(joueur):
    """ Saisie la colonne """
    print("\n\n")
    my_colonne = input("JOUEUR "+str(joueur)+" - CHOIX COLONNE (1 a 7)>")
    return my_colonne

# Permute le joueur
def permute_joueur(j):
    """ permute les joueurs """
    if j == 1:
        j = 2
    else:
        j = 1
    return j

# Affiche un message et quitte le jeu
def affiche_et_quitte(le_message):
    """ Affiche et quitte le jeu """
    print(le_message)
    exit(0)

# Donne le jeton a la position a cote de la position fournie (en fonction de direction)
#     8 1 2
#     7 X 3
#     6 5 4
# retourn un objet avec la position et le contenu de la case (si=3, case inexistante)
def position_direction(lig, col, direction):
    """ Donne le jeton a cote """
    CasePosition.l = lig
    CasePosition.c = col
    CasePosition.case = 3

    if direction == 1 and lig > 0:
        CasePosition.l = lig-1
    elif direction == 2 and lig > 0 and col < 6:
        CasePosition.l = lig-1
        CasePosition.c = col+1
    elif direction == 3 and col < 6:
        CasePosition.c = col+1
    elif direction == 4 and lig < 5 and col < 6:
        CasePosition.l = lig+1
        CasePosition.c = col+1
    elif direction == 5 and lig < 5:
        CasePosition.l = lig+1
    elif direction == 6 and lig < 5 and col > 0:
        CasePosition.l = lig+1
        CasePosition.c = col-1
    elif direction == 7 and col > 0:
        CasePosition.c = col-1
    elif direction == 8 and lig > 0 and col > 0:
        CasePosition.l = lig-1
        CasePosition.c = col-1
    else:
        return CasePosition

    CasePosition.case = plateau[CasePosition.l][CasePosition.c]
    return CasePosition

# Verifie si quelqu un a gagne
# on parcours toute la grille, et on cherche autour de la case sil y a un jeton identique
# (dans la direction donnee), si oui, on regarde dans cette direction
# output
#   vic=0 si personne na gagne
#   vic=1 ou 2 le numero du joueur qui a gagne
def verifie_victoire():
    """ Verifie si l'un des joueurs a gagne """
    vic = 0
    for lig in range(6):
        for col in range(7):
            la_case = plateau[lig][col]
            if la_case != 0:
                print("la_case=" + str(la_case) + " lig=" + str(lig) + " col=" + str(col))
                for direction in range(8):
                    ma_case = position_direction(lig, col, direction + 1)
                    if CasePosition.case == la_case:
                        ma_case = position_direction(CasePosition.l, CasePosition.c, direction + 1)
                        if CasePosition.case == la_case:
                            ma_case = position_direction(CasePosition.l, CasePosition.c, direction + 1)
                            if CasePosition.case == la_case:
                                return la_case
    return vic

# --------------------------------------------------------------------
# Le jeu
# --------------------------------------------------------------------

print(" ----------------------------")
print(" ------- PUISSANCE 4 --------")
print(" ----------------------------")

leJoueur = 1
laColonne = 0
tours = 0
while laColonne < 8:
    # redessiner le plateau
    dessiner_plateau()

    # saisir la colonne pour le joueur en cours
    nLaColonne = int(saisir_num_colonne(leJoueur))
    if ((nLaColonne < 1) or (nLaColonne > 7)):
        affiche_et_quitte("Au revoir !")

    # poser le jeton dans la colonne saisie
    laLigne = poser_jeton(leJoueur, laColonne-1)
    if laLigne == 9:
        print("Colonne deja remplie!")
        # On quitte sans permuter le joueur pour que le joueur en cours puisse rejouer
    else:
        victoire = verifie_victoire()
        if victoire != 0:
            dessiner_plateau()
            print
            affiche_et_quitte("!! VICTOIRE DU JOUEUR "+str(leJoueur)+" !!")

    # changer le joueur
    leJoueur = permute_joueur(leJoueur)

    # compte le nombre de tours, si 42, match nul
    tours += 1
    if tours == 42:
        affiche_et_quitte("Grille pleine - MATCH NUL !!")
