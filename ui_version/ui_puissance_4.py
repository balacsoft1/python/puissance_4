#!/usr/bin/python3                

# permet d'importer les fonctions pour la generation de nombres aleatoires
import random
# tkinter est le framework pour l'interface graphique des jeux
from tkinter import *
# messagebox correspond a une classe pour afficher des messages (pop-up)
from tkinter import messagebox

##############################################################################
# JEU PUISSANCE 4

# cette variable de type tableau contient les donnees de chaque jeton par case
# 0=pas de jeton, 1=jeton player un, 2=jeton player deux
plateau = [
#  0  1  2  3  4  5  6
  [0, 0, 0, 0, 0, 0, 0],  # 0
  [0, 0, 0, 0, 0, 0, 0],  # 1
  [0, 0, 0, 0, 0, 0, 0],  # 2
  [0, 0, 0, 0, 0, 0, 0],  # 3
  [0, 0, 0, 0, 0, 0, 0],  # 4
  [0, 0, 0, 0, 0, 0, 0]]  # 5


##############################################################################
# Class CasePositioon
# permet de stocker une position et une case
# l=ligne
# c=colonne
# case=contenu de la case
class CasePosition:
    l = 0
    c = 0
    case = 0


##############################################################################
# Class Puissance4Window
# il s'agit de la classe principale du jeu "Puissance 4"
class Puissance4Window(Frame):
    
    nbPlayers = 0 # nombre de joueurs (1 ou 2)
    cur_player = 1 # numero du joueur en cours (1 ou 2)
    jetons = 0 # nombre de jetons poses, permet de veririfer si la grille est pleine

    ##################################################################################################################
    # I.A. de l'ordinateur (Intelligence Artificelle)
    def nb_cases_identical(self, player, lig, col):
        vic = 0
        nb_case_identique = 1
        la_case = player
        if la_case != 0:
            for direction in range(8):
                CasePosition = self.position_direction(lig, col, direction + 1)
                if CasePosition.case == la_case:
                    # 1 case identique autour de la case (donc ligne de 2)
                    if nb_case_identique < 2:
                        nb_case_identique = 2
                    CasePosition = self.position_direction(CasePosition.l, CasePosition.c, direction + 1)
                    if CasePosition.case == la_case:
                        # 2 cases identiques autour de la case (donc ligne de 3)
                        if nb_case_identique < 3:
                            nb_case_identique = 3
                        CasePosition = self.position_direction(CasePosition.l, CasePosition.c, direction + 1)
                        if CasePosition.case == la_case:
                            # 3 cases identiques autour de la case (donc ligne de 4) => victoire
                            if nb_case_identique < 4:
                                nb_case_identique = 4

        return nb_case_identique

    # cette fonction permet de determiner la ligne ou le jeton se trouvera dans cette colonne
    def determine_column(self, column_to_analyse):
        if plateau[0][column_to_analyse] != 0:
            # la colonne est pleine
            return 6
        for ligne in range(1, 6):
            if plateau[ligne][column_to_analyse] != 0:
                return ligne - 1
        # si pas trouve, c'est la derniere ligne
        return 5

    # cette fonction determine un nombre de points pour une case donnee
    # en fonction de differentes regles
    def determine_points(self, column_to_analyse, line_to_analyse):
        # nous appliquons plusieur regles pour definir des points, le resultat est la somme des points de chaque regle
        score = 0

        # regle 1 - si on ne peut pas mettre dans cette colonne car elle est pleine, score = 0
        if line_to_analyse == 6:
            return 0

        # regle 2 - chaque colonne du plateau a un nombre de points : 1 2 2 3 2 2 1
        score_column = [1, 2, 2, 3, 2, 2, 1]
        score += score_column[column_to_analyse]

        # on recupere le nombre de cases identiques autour de la case si l'ordinateur la joue et si le player joue
        nb_cases_identiques_joueur = self.nb_cases_identical(1, line_to_analyse, column_to_analyse)
        nb_cases_identiques_ordinateur = self.nb_cases_identical(2, line_to_analyse, column_to_analyse)

        # regle 3 - si l'ordinateur peut gagner en posant le jeton dans cette colonne, on attribue le score max
        if nb_cases_identiques_ordinateur == 4:
            score += 1000

        # regle 4 - si le joueur peut gagner en posant le jeton dans cette colonne, on attribue un score tres fort
        if nb_cases_identiques_joueur == 4:
            score += 800

        # regle 5 - si l'ordinateur peut aligner 3 jetons en posant dans cette colonne, on attribue un score assez fort
        if nb_cases_identiques_ordinateur == 3:
            score += 100

        # regle 5 - si le joueur peut aligner 3 jetons en posant dans cette colonne, on attribue un score assez fort
        if nb_cases_identiques_joueur == 3:
            score += 50

        # regle 6 - si l'ordinateur peut aligner 2 jetons en posant dans cette colonne, on attribue un score correct
        if nb_cases_identiques_ordinateur == 2:
            score += 10

        # regle 7 - si le joueur peut aligner 3 jetons en posant dans cette colonne, on attribue un score correct
        if nb_cases_identiques_joueur == 2:
            score += 5

        # on renvoi le score de la colonne
        return score

    # cette fonction calcul une ponderation aleatoire pour rajouter un peu de changement dans les parties
    def add_alea_ponderation(self, points):
        # determine une ponderation aleatoire entre 0.45 et 2.1 (pour diviser par 2 ou multiplier par 2)
        frand = random.uniform(0.45, 2.1)
        ret_value = int(round(points * frand,0))
        return ret_value

    # cette fonction renvoi la colonne choisie par l'ordinateur
    def ordinateur_joue(self):
        col = 0
        max_points = 0
        points_column = [0, 0, 0, 0, 0, 0, 0]

        # parcours toutes les colonnes pour calculer le point correspondant
        for column_to_analyse in range(7):
            line_to_analyse = self.determine_column(column_to_analyse)
            points = self.determine_points(column_to_analyse, line_to_analyse)
            points_pondere = self.add_alea_ponderation(points)
            points_column[column_to_analyse] = points_pondere

        # recherche la colonne qui a les points les plus eleves
        for column_to_analyse in range(7):
            if points_column[column_to_analyse] > max_points:
                max_points = points_column[column_to_analyse]
                col = column_to_analyse

        return col
    ##################################################################################################################


    ###############################################################################
    # gestions des boutons
    
    # bouton "REGLES DU JEU"
    def my_rules(self):
        messagebox.showinfo("REGLES DU JEU", "Le but du jeu puissance 4 est d'aligner 4 jetons, en ligne, en colonne\
        ou en diagonale. Le plateau comporte 7 colonnes et 6 lignes ou chaque joueur va deposer un jeton qui tombera.")

    # bouton "REDEMARRER LE JEU"
    def restart_game_button(self):
        if self.nbPlayers != 0:
            response = messagebox.askyesno("PUISSANCE 4", "Confirmer ?")
            if response == True:
                self.restart_game()

    def restart_game(self):
        self.nbPlayers = 0
        self.cur_player = 1
        self.jetons = 0
        self.button1.configure(state=NORMAL)
        self.button2.configure(state=NORMAL)
        for i in range(7):
            self.buttonA[i].configure(state=DISABLED)
        for lig in range(6):
            for col in range(7):
                plateau[lig][col] = 0
                self.buttonJeton[lig][col].configure(bg="grey")

    def re_init(self):
        self.button1.configure(state=DISABLED)
        self.button2.configure(state=DISABLED)
        self.dessiner_plateau()
        for i in range(7):
            self.buttonA[i].configure(state=NORMAL)

    # bouton "un joueur"
    def my_1player(self):
        self.nbPlayers = 1
        self.re_init()

    # bouton "2 joueurs"
    def my_2player(self):
        self.nbPlayers = 2
        self.re_init()

    # bouton "Quitter le jeu"
    def quit_jeu(self):
        if self.nbPlayers != 0:
            response = messagebox.askyesno("PUISSANCE 4", "Confirmer ?")
            if response == True:
                self.quit()
        else:
                self.quit()

    # Permute le joueur en cours
    def permute_joueur(self, j):
        if j == 1:
            j = 2
        else:
            j = 1
        return j

    # Poser un jeton dans une colonne donnee
    # =lacolonne si tout s est bien passe
    # =6 si la colonne est pleine
    def poser_jeton(self, couleurJeton, colonneCible):
        if plateau[0][colonneCible] != 0:
            return 6
        for lig in range(1, 6):
            if plateau[lig][colonneCible] != 0:
                plateau[lig - 1][colonneCible] = couleurJeton
                return lig - 1
        plateau[5][colonneCible] = couleurJeton
        return 5

    # dessiner le plateau de jeu
    def dessiner_plateau(self):
        for lig in range(6):
            for col in range(7):
                val = plateau[lig][col]
                # self.buttonJeton[lig][col]["text"] = str(val)
                if val == 1:
                    color = "yellow"
                elif val == 2:
                    color = "red"
                else:
                    color = "white"
                self.buttonJeton[lig][col].configure(bg=color)

        return

    ###############################################################################

    ###############################################################################
    # fonctions diverses pour gerer le jeu
    
    # prend en compte la colonne selectionnee
    def column_selected(self, col):
        laLigne = self.poser_jeton(self.cur_player, col)
        if laLigne == 0:
            # on desactive cette case car la colonne est pleine
            self.buttonA[col].configure(state=DISABLED)
        if laLigne == 6:
            print("Colonne deja remplie!")
            messagebox.showinfo("PUISSANCE 4", "Colonne deja remplie!")
            # On quitte sans permuter le joueur pour que le joueur en cours puisse rejouer
        else:
            self.dessiner_plateau()
            victoire = self.verifie_victoire()
            if victoire != 0:
                messagebox.showinfo("PUISSANCE 4", "VICTOIRE !! joueur "+str(victoire))
                self.restart_game()
            else:
                self.cur_player = self.permute_joueur(self.cur_player)
                self.jetons += 1
                if self.jetons == 42:
                    messagebox.showinfo("PUISSANCE 4", "MATCH NUL !")
                    self.restart_game()
                # verifier si c'est a l'ordinateur de jouer
                if self.nbPlayers == 1:
                    if self.cur_player == 2:
                        # c'est a l'ordinateur de jouer
                        column = self.ordinateur_joue()
                        self.column_selected(column)

    # Donne le jeton a la position a cote de la position fournie (en fonction de direction)
    #     8 1 2
    #     7 X 3
    #     6 5 4
    # retourn un objet avec la position et le contenu de la case (si=3, case inexistante)
    def position_direction(self, lig, col, direction):
        CasePosition.l = lig
        CasePosition.c = col
        CasePosition.case = 3

        if direction == 1 and lig > 0:
            CasePosition.l = lig - 1
        elif direction == 2 and lig > 0 and col < 6:
            CasePosition.l = lig - 1
            CasePosition.c = col + 1
        elif direction == 3 and col < 6:
            CasePosition.c = col + 1
        elif direction == 4 and lig < 5 and col < 6:
            CasePosition.l = lig + 1
            CasePosition.c = col + 1
        elif direction == 5 and lig < 5:
            CasePosition.l = lig + 1
        elif direction == 6 and lig < 5 and col > 0:
            CasePosition.l = lig + 1
            CasePosition.c = col - 1
        elif direction == 7 and col > 0:
            CasePosition.c = col - 1
        elif direction == 8 and lig > 0 and col > 0:
            CasePosition.l = lig - 1
            CasePosition.c = col - 1
        else:
            return CasePosition

        CasePosition.case = plateau[CasePosition.l][CasePosition.c]
        return CasePosition

    # Verifie si quelqu un a gagne
    # on parcours toute la grille, et on cherche autour de la case sil y a un jeton identique
    # (dans la direction donnee), si oui, on regarde dans cette direction
    # output
    #   vic=0 si personne na gagne
    #   vic=1 ou 2 le numero du joueur qui a gagne
    def verifie_victoire(self):
        vic = 0
        for lig in range(6):
            for col in range(7):
                laCase = plateau[lig][col]
                if laCase != 0:
                    # print("laCase="+str(laCase)+" lig="+str(lig)+" col="+str(col))
                    for direction in range(8):
                        CasePosition = self.position_direction(lig, col, direction + 1)
                        if CasePosition.case == laCase:
                            CasePosition = self.position_direction(CasePosition.l, CasePosition.c, direction + 1)
                            if CasePosition.case == laCase:
                                CasePosition = self.position_direction(CasePosition.l, CasePosition.c, direction + 1)
                                if CasePosition.case == laCase:
                                    return laCase
        return vic

    ###############################################################################
    # Constructeur de la classe principale Puissance4Window
    def __init__(self):
        BUTTON_WIDTH = 20

        # dimensionnement de la fenetre, et bloquee pour ne pas pouvoir l'etirer        
        Frame.__init__(self)
        self.master.title('Puissance 4')
        # self.master.geometry('640x480')
        self.master.resizable(width=False, height=False)
        
        # creation de la grille 
        self.master.rowconfigure(0, weight=1)
        self.master.columnconfigure(0, weight=1)
        self.grid(sticky=W + E + N + S)

        # "buttonA" correspond aux 7 boutons en haut pour placer les jetons 
        self.buttonA = [0 for x in range(7)]
        for c in range(7):
            self.buttonA[c] = Button(self, text=str(c+1), state=DISABLED, command=lambda col=c: self.column_selected(col))
            self.buttonA[c].grid(row=0, column=c)

        # "buttonJeton" correspond à l'ensemble des boutons du plateau de 7 colonnes et 6 lignes
        self.buttonJeton = [[0 for x in range(7)] for x in range(6)]
        for l in range(6):
            for c in range(7):
                self.buttonJeton[l][c] = Button(self, text="  ", state=DISABLED, bg="grey")
                self.buttonJeton[l][c].grid(row=l+1, column=c)

        # Bouton pour démarrer le jeu en mode "UN JOUEUR"        
        self.button1 = Button(self, text="UN JOUEUR", command=self.my_1player, width=BUTTON_WIDTH, height=1)
        self.button1.grid(row=0, column=8, columnspan=2)

        # Bouton pour démarrer le jeu en mode "DEUX JOUEURS"        
        self.button2 = Button(self, text="DEUX JOUEURS", command=self.my_2player, width=BUTTON_WIDTH, height=1)
        self.button2.grid(row=1, column=8, columnspan=2)

        # Bouton pour afficher les regles du jeu 
        self.button3 = Button(self, text="REGLES DU JEU", command=self.my_rules, width=BUTTON_WIDTH, height=1)
        self.button3.grid(row=2, column=8, columnspan=2)

        # Bouton pour redemarrer le jeu 
        self.button35 = Button(self, text="RESTART", command=self.restart_game_button, width=BUTTON_WIDTH, height=1)
        self.button35.grid(row=3, column=8, columnspan=2)

        # Bouton pour quitter le jeu 
        self.button4 = Button(self, text="QUITTER", command=self.quit_jeu, width=BUTTON_WIDTH, height=1)
        self.button4.grid(row=4, column=8, columnspan=2)

        self.rowconfigure(1, weight=1)
        self.columnconfigure(1, weight=1)

# lancement de la boucle principale
Puissance4Window().mainloop()
